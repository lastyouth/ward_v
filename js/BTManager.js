function onmessage()
{
	try
	{
		var data = btSocket.readData();
		console.log("received from WARD device : "+data);
		
		if(data[0] == BT_WATCH_EMERGENCY){
			console.log("EMERGENCY from device");
            /* Vibrate for 3 seconds */
            navigator.vibrate(3000);
            tau.changePage("#watch_alert");
            g_alert_timer_id = setTimeout(function(){
            	tau.changePage("#watch_init")
            	clearTimeout(g_alert_timer_id);
            }, 2000);
            
		}else if(data[0] == BT_KEEP_BAG){
			console.log("BT_KEEP_BAG from device");
			g_isBagMode = true;
		}else if(data[0] == BT_WATCH_DUMMY){
			/*g_isWatchArived = true;
			if(g_alert_timer_id == null){
				g_alert_timer_id = setTimeInterval(function(){
					if(!g_isWatchArived){
						 navigator.vibrate(3000);
				            tau.changePage("#watch_alert");
				            g_alert_timer_id = setTimeout(function(){
				            	tau.changePage("#watch_init")
				            	clearTimeout(g_alert_timer_id);
				            }, 2000);
					}else{
						g_isWatchArived = false;
					}
				},3000);
			}*/
		}
		
		if(current_mode == WATCH_MODE)
		{
			// keep alive for target device
			btSocket.writeData([BT_WATCH_DUMMY]);
		}
		
	}catch(e)
	{
		//somethings wrong
		finalizeConnection();
	}
}

function keepAlive()
{
	try
	{
//		console.log("Send keep alive "+[BT_KEEP_ALIVE]);
//		btSocket.writeData([BT_KEEP_ALIVE]);
	}catch(e)
	{
		//somethings wrong
		finalizeConnection();
	}
}

function writeSomething(val)
{
	try
	{
		if (btService.characteristics.length > 0) {
			var characteristic = btService.characteristics[0];
			//var data = new Array(1, 2, 3, 4, 5, 6);
			characteristic.writeValue(val, function() {
				console.log("Value written");
			}, function(e) {
				finalizeConnection();
			});
		}
	}catch(e)
	{
		finalizeConnection();
	}
}

function onRead()
{
	try
	{
		if (btService.characteristics.length > 0) {
            var characteristic = btService.characteristics[0];
            characteristic.readValue(function(val) {
                 console.log("Value read: " + val);
            },function(e){
            	finalizeConnection();
            });
        }
	}catch(e)
	{
		//somethings wrong
		finalizeConnection();
	}
}

function onclose()
{
	console.log("Socket has been closed");
}

function finalizeConnection()
{
	try
	{
		g_WardBluetoothDevice.disconnect();
	}catch(e)
	{
		
	}
//	if(keepAliveIID != -1)
//	{
//		clearInterval(keepAliveIID);
//		keepAliveIID = -1;
//	}
	if(readIID != -1)
	{
		clearInterval(readIID);
		readIID = -1;
	}
	// notify to main for reconnection
	g_mainBTHandler(BT_CONNECTION_LOST);
}

function modeChanged()
{
	if(btSocket == null)
	{
		console.log("socket is null in modeChanged");
	}
	try
	{
		switch(current_mode)
		{
		case INIT_MODE:
			console.log("LOGIC ERROR, CONNECTION ESTABLISHED WHEN INITIALIZE PHASE!");
			break;
		case SEARCH_MODE:
			console.log("Send data "+[BT_SEARCH_MODE]);
			btSocket.writeData([BT_SEARCH_MODE]);
			g_isBagMode = false;
			clearInterval(g_alert_timer_id);
			g_alert_timer_id = null; 
			break;
		case WATCH_MODE:
			console.log("Send data "+[WATCH_MODE]);
			btSocket.writeData([BT_WATCH_MODE]);
			break;
	
		}
	}catch(e)
	{
		console.log("Somethings wrong in modeChanged"+e);
		finalizeConnection();
	}
}

function requestConnection()
{
	console.log("requestConnection");

	if(g_WardBluetoothDevice != null)
	{

		var UUID = g_WardBluetoothDevice.uuids.join("\n");
		console.log("UUID : "+UUID);
		try
		{
			// try to connect

			g_WardBluetoothDevice.connectToServiceByUUID(UUID,function(socket){
				// success to connect
				
				console.log(socket);
				
				btSocket = socket;

				btSocket.onmessage = onmessage;
				btSocket.onclose = onclose;
				
				g_mainBTHandler(BT_CONNECTED_SUCCESSFULLY);

				console.log("Connected with WARD Device");
			
				btSocket.writeData([BT_ALARM_STOP]);
				
//				keepAliveIID = window.setInterval(keepAlive, 2000);

			},function(e){
				console.log("Connection failure.. Try again one more time "+e);
				g_mainBTHandler(BT_CONNECTED_FAILURE);
			});
		}catch(e)
		{

		}
		
	}else
	{
		g_mainBTHandler(BT_NO_WARD_DEVICE_INFO);
	}
}
