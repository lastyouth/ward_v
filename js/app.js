
// test for windows operating system

var bezelTimeoutActivated = false;
// Holds currently registered service record
var chatServiceHandler = null;
// Holds currently open socket
var serviceSocket = null;

function test() {
	console.log("Target Device is not found, Try to discover");
	var discoverDevicesSuccessCallback = {
			onstarted: function() {
				console.log ("Device discovery started...");
			},
			ondevicefound: function(device) {
				console.log("Found device - name: " + device.name + ", Address: "+ device.address);
				
				if(device.name == DEVICE_NAME)
				{
					// we have done it!
					
					console.log("try to bond!!!");
					
					btAdapter.createBonding(device.address,function(device){
						// success
						console.log("Device Name:" + device.name);
					    console.log("Device Address:" + device.address);
					    console.log("Device Service UUIDs:" + device.uuids.join("\n"));
					    
					    g_WardBluetoothDevice = device;
					    
					    delayedPopup('btinitsuccess',false);
					    
					    initializeConnection();
						
					},function(e){
						// error
						console.log("Fail to bond with WARD "+e);
						delayedPopup('btbondingfailure',true);
					});
					
					btAdapter.stopDiscovery(null,null);
				}
			},
			ondevicedisappeared: function(address) {
				console.log("Device disappeared: " + address);
			},
			onfinished: function(devices) {
				console.log("Found Devices");
				var flag = false;
				for (var i = 0; i < devices.length; i++) {
					console.log("Name: " + devices[i].name + ", Address: " + devices[i].address);
					if(flag == false)
					{
						if(devices[i].name == DEVICE_NAME)
						{
							flag = true;
						}
					}
				}
				console.log("Total: " + devices.length);
				if(flag == false)
				{
					delayedPopup('btdiscoveryingfailure',true);
				}
			}
	};
	btAdapter.discoverDevices(discoverDevicesSuccessCallback, null);
}

// main initializing function when app started
function initInternal()
{

	// register handler
	g_mainBTHandler = onMessageFromBTManager;
	// init listener
	document.addEventListener('rotarydetent', bezelListener);
	// BT check
	if(btAdapter == null)
	{
		delayedPopup('btnotenabled',true);
	}
	else
	{
		test();
		// init
		current_mode = INIT_MODE;
	}
}

// request Worker to look at Target device

function initializeConnection()
{
	requestConnection();
}

// page changing relative functions
function initializeSearchMode()
{
	tau.changePage("#search_init");
	
	window.setTimeout(function(){
		tau.changePage("#search_start");
	}, 1500);
	current_mode = SEARCH_MODE;
}
function initializeInitMode()
{
	tau.changePage("#init");
	current_mode = INIT_MODE;
}
function initializeWatchMode()
{
	tau.changePage("#watch_init");
	current_mode = WATCH_MODE;
}
function requestChangeCurrentPage()
{
	//document.removeEventListener('rotarydetent');
	switch(current_mode)
	{
	case INIT_MODE:
		// ignore
		break;
	case SEARCH_MODE:
		initializeWatchMode();
		break;
	case WATCH_MODE:
		initializeSearchMode();
		break;
	}
	modeChanged();
	// must send notification about changing of current mode

	//document.addEventListener('rotarydetent', bezelListener);
}

// delayed popup

function delayedPopup(id,exit)
{
	var popupok = document.getElementById(id+'ok');
	popupok.addEventListener('click',function(){
		tau.closePopup();
		if(exit == true)
		{
			finalize();
		}
	});
	// wait for tau library initialization
	window.setTimeout(function(){
		tau.openPopup('#'+id);
	}, 2000); 
}

// rotatry for bezel control
function bezelListener(event)
{
	// every 5 tick can trigger to switch the current page;
    if(g_Bezelcount != 0 && g_Bezelcount%8 == 0)
    {
    	requestChangeCurrentPage();
    }
    g_Bezelcount++;
    
    // this routine is safe guard for reverting user's behavior
    if(bezelTimeoutActivated == false)
    {
    	bezelTimeoutActivated = true;
    	window.setTimeout(function(){
    		g_Bezelcount = 0;
    		bezelTimeoutActivated = false;
    	}, 2000);
    }
}

// onmessage from worker
function onMessageFromBTManager(detail)
{
	var event = detail;
	
	console.log("Event from BT : "+event);
	
	switch(event)
	{
	case BT_NO_WARD_DEVICE_INFO:
		// FATAL ERROR
		break;
	case BT_CONNECTED_SUCCESSFULLY:
		initializeSearchMode();
		modeChanged();
		break;
	case BT_CONNECTION_LOST:
		initializeInitMode();
		// request reconnection
		initializeConnection();
		break;
	case BT_CONNECTED_FAILURE:
		// try in 3 sec
		window.setTimeout(requestConnection, 3000);
		break;
	}
}

// graceful exit mechanism
function finalize()
{
	tizen.application.getCurrentApplication().exit();
}

( function () {
	
	//console.log("start discover bt devices");
	//btAdapter.discoverDevices(discoverDevicesSuccessCallback, null);

	initInternal();
	window.addEventListener( 'tizenhwkey', function( ev ) {
		console.log(ev.keyName);
		if( ev.keyName === "back" ) {
			console.log("app will be terminated");
			finalize();
		}
	} );
	
	window.addEventListener('touchstart', function(ev){
		if(current_mode == SEARCH_MODE){
			console.log("scren touched in search mode.");
			if(g_isSearchStart){
				tau.changePage("#search_start");
				g_isSearchStart = false;
				btSocket.writeData([BT_ALARM_STOP]);
			}else{
				tau.changePage("#search_stop");
				g_isSearchStart = true;
				btSocket.writeData([BT_SEARCH_START]);
			}
		}
	})
} () );


