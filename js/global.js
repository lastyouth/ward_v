// global variables for all .js files
var g_Bezelcount = 0; // bezel count for user
var btAdapter = null; // btAdapter

try
{
	btAdapter = tizen.bluetooth.getDefaultAdapter();
	//btAdapter = tizen.bluetooth.getLEAdapter();
}catch(e)
{
	btAdapter = null
	console.log(e);
}
// Bluetooth constant
const BT_CONNECTION_REQUEST = "BTCONNECT";
const BT_MODE_CHANGED = "BTMODECHANGED";
const BT_TARGET_DEVICE_FOUND = "BTTARGETDEVICEFOUND";

const BT_SEARCH_MODE = 49;
const BT_SEARCH_START = 80;

const BT_WATCH_MODE = 50;
const BT_WATCH_DUMMY = 87;

const BT_WATCH_EMERGENCY = 71;

const BT_ALARM_STOP = 88;

const BT_KEEP_ALIVE = 72;

const BT_KEEP_BAG = 79;

// in main
// constant
const INIT_MODE = 0;
const SEARCH_MODE = 1;
const WATCH_MODE = 2;

const BT_NO_WARD_DEVICE_INFO = "BTNOINFO";
const BT_CONNECTED_SUCCESSFULLY = "BTCONNECTSUCCESS";
const BT_CONNECTED_FAILURE = "BTCONNECTFAILURE";
const BT_CONNECTION_LOST = "BTCONNECTIONLOST";

const DEVICE_NAME = "WARD";


var btSocket = null;
var btService = null;
var keepAliveIID = -1;
var readIID = -1;
var g_WardBluetoothDevice = null;
var g_mainBTHandler = null;
//this global
var current_mode = INIT_MODE;
var g_isSearchStart = false;
var g_isBagMode = false;

var g_watch_timer = null;
var g_alert_timer_id = null;
var g_isWatchArived = true;
